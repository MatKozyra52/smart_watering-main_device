## Funkcje plików
 - **addDev.html** – element aplikacji webowej - strona dodawania nowych urządzeń do sieci,
 - **connect.html** – element aplikacji webowej - łaczenie z siecią WiFi,
 - **last_timestamp.txt** – plik tekstowy zapisujący wartość ostatniej próbki prognozy w pamięci,
 - **main.html** – element aplikacji webowej - strona główna,
 - **not_set.html** – element aplikacji webowej - komunikat o braku połączenia z siecią WiFi,
 - **rain_forecast.txt** – plik służący do zapisu bieżącej prognozy dotyczącej opadów,
 - **settings.html** – element aplikacji webowej - ustawienia systemu,
 - **settings_style.css** – element aplikacji webowej - styl poszczególnych stron.


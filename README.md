# System podlewania roślin bazujący na informacji z internetowego serwisu pogodowego
Niniejsze repozytorium stanowi uzupełnienie projektu dyplomowego. 
 
## Struktura plików
Przedstawiona struktura plików wygenerowana została automatycznie przez środowisko PlatformIO.  
Folder **"data"** zawiera pliki wgrywane do pamięci SPIFFS mikrokontrolera ESP32. Znajduje się tam kod interfejsu uzytkownika, oraz pliki pełniące role bufora dla parametrów systemowych. **"lib"** przechowuje biblioteki wykorzystane w projekcie - w tym biblioteki autorskie. Natomiast w folderze **"src"** znajduje się główny kod programu - main.cpp  
Plik **platformio.ini** zawiera parametry konfiguracyjne projektu.

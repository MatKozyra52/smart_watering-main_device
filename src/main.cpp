#include <Arduino.h>
#include "MK_INFO_ESP.h"

DATA_MACHINE_ESP machine;
bool newstate = 0;

void rx_routine();
void config_irq();

void setup(void){
  Serial.begin(9600);
  radioInit();
  pinMode(NRF_RX_PIN, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(NRF_RX_PIN), rx_routine, FALLING);
  pinMode(CONFIG_INPUT, INPUT_PULLDOWN);
  attachInterrupt(digitalPinToInterrupt(CONFIG_INPUT), config_irq, FALLING);

  Serial.println("init machine");
  MACHINE_initMachine(&machine);
  printDetails();
}

/* Main loop */
void loop(void) {

  MACHINE_stateMachine(&machine);

  if(newstate) {
    MACHINE_changeState(S_CONFIG, &machine);
    newstate = 0;
  }
}



/* Radio IRQ */
void IRAM_ATTR rx_routine(){
  Serial.print("IRQ");
  if(!machine.newRx){
    char rx_buff[32];
    size_t len = radioRead(rx_buff);
    DATA_rawToPacket((uint8_t*)rx_buff, len, &machine.packetBuffer);
    DATA_printPacket(&machine.packetBuffer);
    machine.newRx = 1;
  }
}

/* External IRQ */
void config_irq(){
  Serial.println("External IRQ");
  newstate = 1;
}
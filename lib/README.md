## Zewnętrzne biblioteki wykorzystane w projekcie
 - **RF24** – obsługa modułu radiowego nRF24L01.
 - **Esp32WebServer** – wykorzystanie ESP jako serwera – podstawa implementacji aplikacji webowej.

## Biblioteki autorskie - MK_LIBS
 - **MK_EEPROM_DATA** – wykorzystywana do obsługi pamięci EEPROM celem zapisania bądź wczytania parametrów modyfikowanych pomiędzy kolejnymi uruchomieniami urządzenia.
 - **MK_INFO_ESP** – część logiczna zawierająca maszynę stanów, oraz przypisane im akcje. Najbardziej rozbudowany plik spajający pozostałe biblioteki.
 - **MK_TIME_API** – prosta nakładka na bibliotekę „time.h” wykorzystywaną do synchronizacji.
 - **MK_VALVE_CONTROL** – funkcje sterujące elektrozaworami.
 - **MK_WEATHER_API** – komunikacja z internetowym serwisem pogody, oraz przetwarzanie otrzymanej prognozy.
 - **private.h** – definiuje parametry sieci tworzonej na etapie konfiguracji (SSID, hasło, adresacja).

 


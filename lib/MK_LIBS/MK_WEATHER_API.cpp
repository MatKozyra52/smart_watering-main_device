#include "MK_WEATHER_API.h"
#include <string>
#include "MK_TIME_API.h"
#include "SPIFFS.h"

#define HOUR_S 3600
const char* API_KEY = "bb0a4edf79611d0cd6ea029063d33586";

String rain_forecast;
String last_timestamp;

WEATHER_CallParams_s* API_defaultParams(float lat, float lon){
    WEATHER_CallParams_s* params = (WEATHER_CallParams_s*) malloc(sizeof(WEATHER_CallParams_s));

    //appid
    memset(params->appid,0,APPID_LEN);
    memcpy(params->appid, API_KEY, APPID_LEN);
    params->forecastTime = CURRENT;
    params->lat = lat;
    params->lon = lon;
    params->units = METRIC;

    return params;
}

int API_httpGetRequest(WEATHER_CallParams_s* callParams, String* forecast) {

    WiFiClient client;
    HTTPClient http;
    char apiReq[200] = "http://api.openweathermap.org/data/2.5/onecall?";
    size_t buffer_size = 64;
    char buffer[buffer_size];
    memset(buffer,0,buffer_size);
    
    //append localization
    sprintf(apiReq,"%slat=%.2f&lon=%.2f", apiReq, callParams->lat, callParams->lon);  
    
    //forecast time
    if(callParams->forecastTime != CURRENT) sprintf(buffer,"%scurrent,", buffer);
    if(callParams->forecastTime != MINUTELY) sprintf(buffer,"%sminutely,", buffer);
    if(callParams->forecastTime != HOURLY) sprintf(buffer,"%shourly,", buffer);
    if(callParams->forecastTime != DAILY) sprintf(buffer,"%sdaily,", buffer);
    if(callParams->forecastTime != ALERTS) sprintf(buffer,"%salerts,", buffer);
    for(size_t k = buffer_size; k>1; k--){
        if(buffer[k-1]==',') {buffer[k-1]=0; break;}
    }
    sprintf(apiReq,"%s&exclude=%s", apiReq, buffer);

    //units
    switch (callParams->units){
    case STANDARD:
        sprintf(apiReq,"%s&units=%s", apiReq, "standard");
        break;
    case IMPERIAL:
        sprintf(apiReq,"%s&units=%s", apiReq, "imperial");
        break;
    case METRIC:   
    default:
        sprintf(apiReq,"%s&units=%s", apiReq, "metric");
        break;
    }

    //appid
    memset(buffer,0,buffer_size);
    memcpy(buffer, callParams->appid, APPID_LEN);
    sprintf(apiReq,"%s&appid=%s", apiReq, buffer);

    Serial.println(apiReq);

    http.begin(client, apiReq);
    int httpResponseCode = http.GET();

    if (httpResponseCode>0) {
        Serial.print("HTTP Response code: ");
        Serial.println(httpResponseCode);
        Serial.println(http.getSize());
        int forecast_size = (http.getSize()+1)*sizeof(char);
        *forecast = http.getString();
        http.end();
        return forecast_size;
    }
    else {
        Serial.print("Error code: ");
        Serial.println(httpResponseCode);
    }
    // Free resources
    http.end();
    return 0;
}

void API_updateRainForecast(WEATHER_CallParams_s* callParams){
    String data;
    rain_forecast  ="";
    API_httpGetRequest(callParams, &data);
    int pos = data.indexOf("\"dt\":", 0);
    while(pos != -1){
        int first_rain = data.indexOf("\"rain\":", pos+1);
        int next_dt = data.indexOf("\"dt\":", pos+1);
        if(first_rain < next_dt && first_rain != -1){                           // rain at this stamp
            int end_time = data.indexOf(',', pos);
            int end_rain = data.indexOf('}', first_rain);
            String time = data.substring(pos, end_time+1);
            String rain = data.substring(first_rain, end_rain+1);
            rain_forecast += (time + rain + '\n');         
        }          
        
        if (next_dt == -1){
            int end_time = data.indexOf(',', pos);
            String time = data.substring(pos, end_time+1);
            last_timestamp = time;
        }   

        pos = next_dt;
    }
}

void API_getRainForecast(String* rainForecast){
    *rainForecast = rain_forecast;
}

void API_setRainForecast(String* rainForecast){
    rain_forecast = *rainForecast;
}

void API_printRainForecast(){
    Serial.println(rain_forecast);
}

void API_getLastTimestamp(String* lastTimestamp){
    *lastTimestamp = last_timestamp;
}

void API_setLastTimestamp(String* lastTimestamp){
    last_timestamp = *lastTimestamp;
}

void API_printLastTimestamp(){
    Serial.println(last_timestamp);
}

bool API_needUpdate(){
    time_t time_now= TIME_getSec();
    int pos = last_timestamp.indexOf(':', 0);
    if(pos != -1){
        int end_time = last_timestamp.indexOf(',', 0);
        String time = last_timestamp.substring(pos+1, end_time);
        time_t last_forecast = time.toInt();
        if((last_forecast - time_now) < (HOUR_S * 24)){                   // we need to keep forecast for 24h
            return 1;               
        }else return 0;
    }
    return 1;
}

float API_rainInNext24h(){
    float sum =0.0;

    time_t time_now= TIME_getSec();
    int rain_time = rain_forecast.indexOf("\"dt\"", 0);
    while(rain_time != -1){
        int end_time = rain_forecast.indexOf(',', rain_time);
        String time = rain_forecast.substring(rain_time + 5, end_time);
        time_t dt = time.toInt();
        if((dt > time_now) && (dt - time_now) < (HOUR_S * 24)){         // only values for next 24h
            int rain_value = rain_forecast.indexOf("\"1h\":", rain_time);
            String value = rain_forecast.substring(rain_value + 5, rain_value + 9);
            float sample = value.toFloat();
            sum = sum + sample;
        }
        rain_time = rain_forecast.indexOf("\"dt\"", rain_time + 1);
    }
    return sum;
}
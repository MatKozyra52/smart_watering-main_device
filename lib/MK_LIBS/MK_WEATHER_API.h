/*
 * WEATHER library
 * Created: 10.2021
 * Author : Mateusz Kozyra
 * Purpose : Part of project made at AGH UST.
 * Description : This library is part of Smart Watering System
 * Features : External weather service comunication, and data handling
 */ 


#ifndef WEATHER_API_H_
#define WEATHER_API_H_

#include <Arduino.h>
#include <HTTPClient.h>
#include "stdbool.h"

#define APPID_LEN 32

/* Forecast request parameters **************/
typedef enum {
    CURRENT = 1,
    MINUTELY,
    HOURLY,
    DAILY,
    ALERTS
} WEATHER_ForecastTime_t;

typedef enum {
    STANDARD = 1,
    METRIC,
    IMPERIAL
} WEATHER_Units_t;

typedef struct WEATHER_PARAMS{
    char appid[APPID_LEN];
    float lon;
    float lat;
    WEATHER_ForecastTime_t forecastTime;
    WEATHER_Units_t units;
    char* lang;
} WEATHER_CallParams_s;
/*********************************************/

/************************************************************************
 @brief Get default forecast request parameters for selected localization
 @return Structure pointer necessery for weather request
************************************************************************/
WEATHER_CallParams_s* API_defaultParams(float lat, float lon);

/************************************************************************
 @brief Get full forecast
 @param callParams Request details
 @param forecast Output forecast buffer
************************************************************************/
int API_httpGetRequest(WEATHER_CallParams_s* callParams, String* forecast);

/************************************************************************
 @brief Handling rain data - parsed from full forcast
************************************************************************/
void API_updateRainForecast(WEATHER_CallParams_s* callParams);
void API_printRainForecast();
void API_getRainForecast(String* rainForecast);
void API_setRainForecast(String* rainForecast);

/************************************************************************
 @brief Sum of rain in next 24h
************************************************************************/
float API_rainInNext24h();

/************************************************************************
 @brief Last forecast timestamp handling
************************************************************************/
void API_printLastTimestamp();
void API_getLastTimestamp(String* lastTimestamp);
void API_setLastTimestamp(String* lastTimestamp);
bool API_needUpdate();

#endif //WEATHER_API_H_
#include "MK_EEPROM_DATA.h"

/* EEPROM data defines */
#define EP_WIFI_MODE    0
#define EP_SSID         1
#define SSID_LEN        32
#define EP_PASS         EP_SSID + SSID_LEN
#define PASS_LEN        64
#define EP_DATA_FLAG    EP_PASS + PASS_LEN
#define EP_DATA         EP_DATA_FLAG + 1
#define EEPROM_SIZE     512 //EP_PASS + PASS_LEN
/* ******************** */

void EEPROM_init(){
  EEPROM.begin(EEPROM_SIZE);
}

bool EEPROM_getWifi(){
    return EEPROM.readByte(EP_WIFI_MODE);
}

void EEPROM_setWifi(bool mode){
    EEPROM.writeByte(EP_WIFI_MODE, mode);
    EEPROM.commit();
}

String EEPROM_getSSID(){
    return EEPROM.readString(EP_SSID);
}

void EEPROM_setSSID(String ssid){
   EEPROM.writeString(EP_SSID, ssid);
   EEPROM.commit();
}

String EEPROM_getPass(){
    return EEPROM.readString(EP_PASS);
}

void EEPROM_setPass(String pass){
    EEPROM.writeString(EP_PASS, pass);
    EEPROM.commit();
}

bool EEPROM_getData(void *data, size_t size){
    if(EEPROM.readByte(EP_DATA_FLAG) == 1){
        EEPROM.readBytes(EP_DATA, data, size);
        return 1;
    }else return 0;
}

void EEPROM_setData(void *data, size_t size){
    EEPROM.writeByte(EP_DATA_FLAG, 1);
    EEPROM.writeBytes(EP_DATA, data, size);
    EEPROM.commit();
}

void EEPROM_clearData(){
    EEPROM.writeByte(EP_DATA_FLAG, 0);
    EEPROM.commit();
}

/*
 * TIME library
 * Created: 12.2021
 * Author : Mateusz Kozyra
 * Purpose : Part of project made at AGH UST.
 * Description : This library is part of Smart Watering System
 * Features : Time menagement, synchronization
 */ 

#ifndef TIME_API_H_
#define TIME_API_H_

#include <Arduino.h>
#include "time.h"
#include "stdbool.h"

/************************************************************************
 @brief Get actual time in diferent forms
************************************************************************/
void TIME_printTime();
time_t TIME_getSec();
struct tm TIME_getTime();


#endif //TIME_API_H_
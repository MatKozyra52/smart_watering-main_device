/*
 * VALVE CONTROL library
 * Created: 12.2021
 * Author : Mateusz Kozyra
 * Purpose : Part of project made at AGH UST.
 * Description : This library is part of Smart Watering System
 * Features : Valve control - change of state, sleep modes
 */ 

#ifndef VALVE_CONTROL_H_
#define VALVE_CONTROL_H_

#include <Arduino.h>
#include "stdbool.h"

/* Pinout */
#define VALVE1A 21
#define VALVE1B 19
#define VALVE2A 18
#define VALVE2B 5
#define VALVE3A 17
#define VALVE3B 16

#define CLOSE   0
#define OPEN    1

void VALVE_init();
void VALVE_action(uint8_t nr, bool open);
void VALVE_closeAll();

/************************************************************************
 @brief Hold-down output pins in sleep mode to reduce pasive current
************************************************************************/
void VALVE_offState();

/************************************************************************
 @brief Disable output pins hold-down
************************************************************************/
void VALVE_wakeUp();

#endif //VALVE_CONTROL_H_
#include "MK_VALVE_CONTROL.h"

void VALVE_action(uint8_t nr, bool open){
  uint8_t valvePin[] = {VALVE1A, VALVE1B,VALVE2A, VALVE2B,VALVE3A, VALVE3B};
  digitalWrite(valvePin[2*nr - 2], open);
  digitalWrite(valvePin[2*nr - 1], !open);
  delay(30);
  digitalWrite(valvePin[2*nr - 2], LOW);
  digitalWrite(valvePin[2*nr - 1], LOW);  
}

void VALVE_init(){
  pinMode(VALVE1A, OUTPUT);
  pinMode(VALVE1B, OUTPUT);
  pinMode(VALVE2A, OUTPUT);
  pinMode(VALVE2B, OUTPUT);
  pinMode(VALVE3A, OUTPUT);
  pinMode(VALVE3B, OUTPUT);
  VALVE_wakeUp();
}

void VALVE_closeAll(){
  VALVE_action(1,CLOSE);
  VALVE_action(2,CLOSE);
  VALVE_action(3,CLOSE);
}

void VALVE_offState(){
  digitalWrite(VALVE1A, 0);
  digitalWrite(VALVE1B, 0);
  digitalWrite(VALVE2A, 0);
  digitalWrite(VALVE2B, 0);
  digitalWrite(VALVE3A, 0);
  digitalWrite(VALVE3B, 0);
  gpio_hold_en((gpio_num_t)VALVE1A);
  gpio_hold_en((gpio_num_t)VALVE1B);
  gpio_hold_en((gpio_num_t)VALVE2A);
  gpio_hold_en((gpio_num_t)VALVE2B);
  gpio_hold_en((gpio_num_t)VALVE3A);
  gpio_hold_en((gpio_num_t)VALVE3B);
  gpio_deep_sleep_hold_en();
}

void VALVE_wakeUp(){
  gpio_hold_dis((gpio_num_t)VALVE1A);
  gpio_hold_dis((gpio_num_t)VALVE1B);
  gpio_hold_dis((gpio_num_t)VALVE2A);
  gpio_hold_dis((gpio_num_t)VALVE2B);
  gpio_hold_dis((gpio_num_t)VALVE3A);
  gpio_hold_dis((gpio_num_t)VALVE3B);
}
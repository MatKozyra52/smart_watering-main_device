/*
 * ESP INFO library
 * Created: 10.2021
 * Author : Mateusz Kozyra
 * Purpose : Part of project made at AGH UST.
 * Description : This library is part of Smart Watering System
 * Features : Packet handling, State machine of the device, Web app, Sleep mode, Watering
 */ 


#ifndef MK_INFO_ESP_H_
#define MK_INFO_ESP_H_

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "time.h"
#include "MK_VALVE_CONTROL.h"

/* Pinout */
#define CONFIG_INPUT  	34
#define LED_OUT			2
#define NRF_RX_PIN		26

/* Packet defines */
#define OVERHEAD_LEN 	2
#define HEADER_REQ		7
#define HEADER_ACTION	4
#define HEADER_CMD		0
#define HEADER_CMD_MASK	0x0F

/**** CMD ********************/
#define CMD_PING		0b0001
#define CMD_SET_RX_ADDR	0b0010
#define CMD_SET_TX_ADDR	0b0011
#define CMD_GET_DATA	0b0100
#define CMD_SET_NODE_ID	0b0101
#define CMD_INIT		0b1110
#define CMD_GO_TO_SLEEP	0b1111
/**** CMD ********************/
#define CMD_REQUEST		1
#define CMD_RESPONSE	0

#ifdef __cplusplus
extern "C" {
#endif


typedef struct _DATA_HEADER{
	bool request;					// 7 - Request/Response
									// 6:5 - Not used yet
	bool action;					// 4 - Action/Information - not used
	uint8_t cmd_id;					// 3:0 - Command ID
	} DATA_HEADER;

typedef struct _DATA_PACKET{
	uint8_t node_id;
	DATA_HEADER header;
	uint8_t payload[30];
	} DATA_PACKET;

/* Sensor values struct */
typedef struct _DATA_VALUES{
	uint16_t temperature;			// for future developent
	uint16_t humidity;				// for future developent
	uint16_t moisture;
	}DATA_VALUES;

/* States of machine */
typedef enum {
	S_CONFIG,
	S_WAIT,
	S_PACKET_HANDLE_LISTEN,			// listen multicast
	S_PACKET_HANDLE_DATA,			// 1v1 packet exchange
	S_PACKET_HANDLE_SLEEP,			// end of transmission - send to sleep
	S_VALVE
	}MACHINE_STATE_ESP;
	
	
/* Struct for local machine of states */	
typedef struct _DATA_MACHINE_ESP{
	bool newRx;						// new packet indicator
	DATA_PACKET packetBuffer;		// last packet
	MACHINE_STATE_ESP state;		// actual machine state
}DATA_MACHINE_ESP;

	
/******************************************************************
 * NRF24L01 CONTROL - Modes/ States/ Info
******************************************************************/
void openReadingPipe(uint8_t pipe, uint8_t* addr);
void openWritingPipe(uint8_t* addr);
void closeReadingPipe(uint8_t pipe);
void closeAllPipes();
void radioInit();
void printDetails();
void startListening();
void stopListening();
bool radioWrite(uint8_t* data);
bool radioWritePacket(DATA_PACKET *packet);
bool available();
uint8_t getDynamicPayloadSize();
size_t radioRead(void* buf);
/*****************************************************************/

/******************************************************************
 * Radio communication - Packet handling 
******************************************************************/
bool DATA_action(DATA_PACKET* packet);
bool DATA_actionInit(DATA_PACKET* packet);
bool DATA_actionPing(DATA_PACKET* packet);
bool DATA_actionGetData(DATA_PACKET* packet);
bool DATA_actionSetTxAddr(DATA_PACKET* packet);
bool DATA_actionSetNodeId(DATA_PACKET* packet);
bool DATA_actionGoToSleep(DATA_PACKET* packet);
bool DATA_rawToPacket(uint8_t* raw_data, size_t len, DATA_PACKET* packet);
size_t DATA_packetToRaw(struct _DATA_PACKET* packet, uint8_t** raw_data);
bool DATA_sendPing();
bool DATA_sendDataReq();
bool DATA_sendSleep(uint8_t min_time);
void DATA_createPacket(DATA_PACKET* packet, bool act, uint8_t cmd, bool request);
void DATA_createPacketRequest(DATA_PACKET* packet, bool act, uint8_t cmd);
void DATA_createPacketResponse(DATA_PACKET* packet, bool act, uint8_t cmd);
void DATA_printPacket(DATA_PACKET* packet);
/*****************************************************************/


/******************************************************************
 * State machine procedures
******************************************************************/
void MACHINE_initMachine(DATA_MACHINE_ESP *stateMachine);
void MACHINE_stateMachine(DATA_MACHINE_ESP *machineState);
void MACHINE_changeState(MACHINE_STATE_ESP setState, DATA_MACHINE_ESP *machineState);
void MASTER_saveForecast();
void MASTER_saveConfig();
void MASTER_loadForecast();
bool MASTER_loadConfig();
uint8_t MASTER_addNewNode();
/*****************************************************************/

/******************************************************************
 * WiFi control
******************************************************************/
void WIFI_modeAP();
bool WIFI_modeSTA();
void WIFI_disconnect();
bool WIFI_userConnected();
/*****************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* MK_INFO_ESP_H_ */
#include "MK_TIME_API.h"

struct tm TIME_getTime(){
	struct tm timeinfo;
	if(!getLocalTime(&timeinfo)){
		Serial.println("Failed to obtain time");
	return timeinfo;
	}
}

time_t TIME_getSec(){
	struct tm timeinfo;
	timeinfo = TIME_getTime();
	return mktime(&timeinfo);	
}

void TIME_printTime(){
  struct tm timeinfo;
  if(!getLocalTime(&timeinfo)){
    Serial.println("Failed to obtain time");
    return;
  }
  Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");
}
/*
 * EEPROM DATA library
 * Created: 11.2021
 * Author : Mateusz Kozyra
 * Purpose : Part of project made at AGH UST.
 * Description : This library is part of Smart Watering System
 * Features : EEPROM operations - saving and loading
 */ 


#ifndef MK_EEPROM_DATA_H_
#define MK_EEPROM_DATA_H_


#include <EEPROM.h>


void EEPROM_init();
void EEPROM_clearData();

/************************************************************************
 @brief Setter and getter for WiFi indicator.
 @param bool Indicates if WiFi data saved in EEPROM
************************************************************************/
bool EEPROM_getWifi();
void EEPROM_setWifi(bool mode);

/************************************************************************
 @brief Setter and getter for WiFi SSID and password
************************************************************************/
String EEPROM_getSSID();
void EEPROM_setSSID(String ssid);
String EEPROM_getPass();
void EEPROM_setPass(String pass);

/************************************************************************
 @brief Setter and getter for other data saved in EEPROM
 @param[in] data Pointer to data block
 @param[in] size Size of data t save
************************************************************************/
bool EEPROM_getData(void * data, size_t size);
void EEPROM_setData(void * data, size_t size);

#endif //MK_EEPROM_DATA_H_
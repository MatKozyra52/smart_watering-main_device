/*
 * MK_INFO.c
 *
 * Created: 28.10.2021 11:08:41
 *  Author: Acer
 */ 


#include "MK_INFO_ESP.h"
#include <stdio.h>
#include <stdint.h>
#include  <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include "MK_EEPROM_DATA.h"

#include "SPIFFS.h"
#include <WiFi.h>
#include "ESP32WebServer.h"
#include "private.h"
#include "MK_TIME_API.h"
#include "MK_WEATHER_API.h"
// SERVER PART
ESP32WebServer server(80);
String SPIFFS_readFile(String filename);   
///////////////////////////////////

// NRF
#define NRF_CHANNEL		0x74
#define SLAVE_NR_MAX	10
uint8_t default_address[] = { 0xe7,0xe7,0xe7,0xe7,0xe7 };
uint8_t local_address[] = { 0xe6,0xe6,0xe6,0xe6,0xe6 };
uint8_t local_group[] = { 0xe6,0xe6,0xe6,0xe6,0x0 };

typedef struct _DEVICE{
	bool connected;	
	uint8_t nodeId;
	uint8_t nodeAddress[5];
	DATA_VALUES sensorValues;
	uint8_t sleepTime;
	uint8_t pumpId;
	time_t wakeStamp;
	} DEVICE;

typedef struct _CONFIG{
	DEVICE localDevice;
	DEVICE slaveDevices[SLAVE_NR_MAX];
	uint8_t slaveNr;
	bool timetable[24];
	uint8_t moisture_min;
	float latitude;
	float longitude;
} CONFIG;

void DATA_printDEVICE(DEVICE Device);

DEVICE localDevice;
DEVICE slaveDevices[SLAVE_NR_MAX];
uint8_t slaveNr = 0;

// system
#define SAMPLE_INTERVAL_WATERING 1
#define SAMPLE_INTERVAL 60

bool timetable[24];
uint8_t moisture_min;
float latitude;
float longitude;

// NTP
const char* ntpServer = "pool.ntp.org";
const long  gmtOffset_sec = 3600;
const int   daylightOffset_sec = 3600;


#define WAKE_SENSOR	1
#define WAKE_VALVE	2

#define SOIL_MOISTURE_MAX	100
#define SOIL_MOISTURE_MIN	600

#define RAIN_MIN	20.0




RF24 radio(27, 15, 14, 12, 13);

void printDetails(){
	radio.printDetails();
}

void startListening(){
	radio.startListening();
}

void stopListening(){
	radio.stopListening();
}

bool radioWrite(uint8_t* data){
	stopListening();
	radio.maskIRQ(1,1,1);
	bool result = radio.write(data, strlen((char*)data));
	radio.maskIRQ(1,1,0);
	startListening();
	return result;
}

bool radioWritePacket(DATA_PACKET *packet){
	//SEND
	uint8_t *tx_mess;
	uint8_t len = DATA_packetToRaw(packet, &tx_mess);
	uint8_t stat = radioWrite(tx_mess);
	Serial.println("Sending packet");
	DATA_printPacket(packet);
	free(tx_mess);
	return stat;
}

bool available(){
	return radio.available();
}

size_t radioRead(void* buf){
	size_t len = getDynamicPayloadSize();
	radio.read(buf,len);
	radio.flush_rx();
	return len;
}

uint8_t getDynamicPayloadSize(){
	return radio.getDynamicPayloadSize();
}

void openReadingPipe(uint8_t pipe, uint8_t* addr){
  uint8_t address[] = { addr[4],addr[3],addr[2],addr[1],addr[0] };
  radio.openReadingPipe(pipe,address);
}

void openWritingPipe(uint8_t* addr){
  uint8_t address[] = { addr[4],addr[3],addr[2],addr[1],addr[0] };
  radio.openWritingPipe(address);
  radio.openReadingPipe(0,address);
}

void closeReadingPipe(uint8_t pipe){
	radio.closeReadingPipe(pipe);
}

void closeAllPipes(){
	for(size_t k = 0; k<6; k++){
		closeReadingPipe(k);
	}
}

void radioInit(){
  radio.begin();
  radio.setChannel(NRF_CHANNEL);
  radio.enableDynamicPayloads();
  radio.setCRCLength(RF24_CRC_16);
  radio.setDataRate(RF24_2MBPS);
  //Turn on RX IRQ
  radio.maskIRQ(1,1,0);
  openReadingPipe(0,default_address);
  openReadingPipe(1,local_address);
  openWritingPipe(default_address);
}


bool DATA_rawToPacket(uint8_t* raw_data, size_t len, DATA_PACKET* packet){

    if (len < OVERHEAD_LEN) return 0;

    packet->node_id = raw_data[0];
    uint8_t head = raw_data[1];
    packet->header.request = (head & (1 << HEADER_REQ)) >> HEADER_REQ;
    packet->header.action = (head & (1 << HEADER_ACTION)) >> HEADER_ACTION;
    packet->header.cmd_id = head & HEADER_CMD_MASK;
    memset(packet->payload, 0, 30);
    memcpy(packet->payload, raw_data+OVERHEAD_LEN, len-OVERHEAD_LEN);
    return 1;
}

size_t DATA_packetToRaw(DATA_PACKET* packet, uint8_t** raw_data){
    
    size_t pay_len = strlen((char*)packet->payload);
    size_t raw_len = pay_len + OVERHEAD_LEN + 1;
    uint8_t* raw = (uint8_t*)malloc(raw_len*sizeof(uint8_t));
    memset(raw, 0, raw_len*sizeof(uint8_t));

    raw[0]= packet->node_id;

    uint8_t head = 0;
    head |= (packet->header.request << HEADER_REQ);
    head |= (packet->header.action << HEADER_ACTION);
    head |= (packet->header.cmd_id);
    raw[1] = head;
    
    memcpy(raw+OVERHEAD_LEN, packet->payload, pay_len);
    *raw_data = raw;
    return raw_len;
}

void DATA_printPacket(DATA_PACKET* packet){
	Serial.printf("Request:%d from %d\tCMD: %X\tPayload:%s\n\r", packet->header.request, packet->node_id, packet->header.cmd_id, packet->payload);
}

bool DATA_action(DATA_PACKET* packet){
	DATA_HEADER head = packet->header;
	bool result = 0;
	
	switch(head.cmd_id){
		/** INIT ***********************/
		case CMD_INIT:;
		result = DATA_actionInit(packet);
		
		if(result) Serial.printf("DATA_actionInit req\n\r");	
		else Serial.printf("DATA_actionInit response\n\r");
		
		return result;
		break;
		
		/** PING ***********************/
		case CMD_PING:;
		result = DATA_actionPing(packet);
		
		if(result) Serial.printf("DATA_actionPing req\n\r");	
		else Serial.printf("DATA_actionPing response\n\r");
		
		return result;
		break;
		
		/** DATA ***********************/
		case CMD_GET_DATA:;
		result = DATA_actionGetData(packet);
		
		if(result) Serial.printf("DATA_actionGetData req\n\r");
		else Serial.printf("DATA_actionGetData response\n\r");
		
		return result;
		break;
		
		/** SET_TX_ADDR ***********************/
		case CMD_SET_TX_ADDR:;
		result = DATA_actionSetTxAddr(packet);

		if(result) Serial.printf("DATA_setTxAddr req\n\r");
		else Serial.printf("DATA_setTxAddr response\n\r");
		
		return result;
		break;
		
		/** SET_NODE_ID ***********************/
		case CMD_SET_NODE_ID:;
		result = DATA_actionSetNodeId(packet);
		
		if(result) Serial.printf("DATA_actionSetNodeId req\n\r");
		else Serial.printf("DATA_actionSetNodeId response\n\r");
		
		return result;
		break;
		
		/** SLEEP ***********************/
		case CMD_GO_TO_SLEEP:;
		result = DATA_actionGoToSleep(packet);
		
		if(result) Serial.printf("DATA_actionGoToSleep req\n\r");
		else Serial.printf("DATA_actionGoToSleep response\n\r");
		
		return result;
		break;
		
		default:
		return 0;
		break;
	}
}

////////////// ACTION CALLBACKS /////////////////////////
bool DATA_actionInit(DATA_PACKET* packet){
	if(!packet->header.request) return false;

	DATA_createPacketResponse(packet, 0, CMD_INIT);
	uint8_t newSlave = MASTER_addNewNode();
	if(newSlave){
		memcpy(packet->payload, &slaveDevices[newSlave], sizeof(DEVICE));
		return 1;
	}
	return 0;
}

bool DATA_actionPing(DATA_PACKET* packet){
	if(!packet->header.request) return false;

	DATA_createPacketResponse(packet, 0, CMD_PING);
	sprintf((char*)packet->payload, "pong");
	//if request we have to response
	return 1;
}

// send local data to MASTER
bool DATA_actionGetData(DATA_PACKET* packet){
	if(!packet->header.request) return false;
	
	DATA_createPacketResponse(packet, 0, CMD_GET_DATA);
	memcpy(packet->payload, &localDevice.sensorValues, sizeof(DATA_VALUES));
	
	//if request we have to response
	return 1;
}

bool DATA_actionSetTxAddr(DATA_PACKET* packet){
	if(!packet->header.request) return false;
	
	openWritingPipe(packet->payload);
	Serial.printf("address changed\n\r");
	radio.printDetails();
	
	DATA_createPacketResponse(packet, 0, CMD_SET_TX_ADDR);
	sprintf((char*)(packet->payload), "ok");
	
	return 0;
}

bool DATA_actionSetNodeId(DATA_PACKET* packet){
	if(!packet->header.request) return false;
	
	localDevice.nodeId = packet->payload[0];
	
	DATA_createPacketResponse(packet, 0, CMD_SET_NODE_ID);
	sprintf((char*)(packet->payload), "ok");
	
	return 1;
}

bool DATA_actionGoToSleep(DATA_PACKET* packet){
	if(!packet->header.request) return false;
	
	localDevice.sleepTime = packet->payload[0];
	
	DATA_createPacketResponse(packet, 0, CMD_GO_TO_SLEEP);
	sprintf((char*)(packet->payload), "ok");
	
	return 1;
}

void DATA_createPacket(DATA_PACKET* packet, bool act, uint8_t cmd, bool request){
	uint8_t id = packet->node_id;
	if (0 == id) id = 1;
	memset(packet, 0, sizeof(DATA_PACKET));
	
	packet->header.action = act;
	packet->header.cmd_id = cmd;
	packet->header.request = request;
	packet->node_id = 1;	//packet send by master have 1==ID used in slave filter 
}

void DATA_createPacketRequest(DATA_PACKET* packet, bool act, uint8_t cmd){
	DATA_createPacket(packet, act, cmd, CMD_REQUEST);
}

void DATA_createPacketResponse(DATA_PACKET* packet, bool act, uint8_t cmd){
	DATA_createPacket(packet, act, cmd, CMD_RESPONSE);
}

bool DATA_sendPing(){
	DATA_PACKET packet;
	DATA_createPacketRequest(&packet, 1, CMD_PING);
	sprintf((char*)(packet.payload), "esp ping");
	
	return radioWritePacket(&packet);
}

bool DATA_sendDataReq(){
	DATA_PACKET packet;
	DATA_createPacketRequest(&packet, 1, CMD_GET_DATA);

	return radioWritePacket(&packet);
}


bool DATA_sendSleep(uint8_t min_time){
	DATA_PACKET packet;
	DATA_createPacketRequest(&packet, 1, CMD_GO_TO_SLEEP);
	packet.payload[0] = min_time;

	return radioWritePacket(&packet);
}

/////////////////////////////// STATE MACHINE ////////////////////////////////
//AP

bool SPIFFS_init(){
	if(!SPIFFS.begin(true)){
		Serial.println("An Error has occurred while mounting SPIFFS");
		return 0;
	}
	return 1;
}
String SPIFFS_readFile(String filename)                //from SPIFFS
{
  String text;
  File file = SPIFFS.open(filename, "r");
  while (file.available())
    text += (char)file.read();
  file.close();
  return text;
}

void SPIFFS_writeFile(String filename, String data)                //to SPIFFS
{
	File file = SPIFFS.open(filename, "w");
	if(!file){
		Serial.println("Failed to open test file");
		return;
	} else {
		file.print(data);
		file.close();
	}
}
////////////// WEB APP /////////////
// main page
void handle_root(){
	server.send(200, "text/html", SPIFFS_readFile("/main.html"));
	Serial.println(EEPROM_getSSID());
	Serial.println(EEPROM_getPass());
}

bool change_state = 0;
void handle_save(){
	if(EEPROM_getWifi()){	
		change_state = 1;
		// update time
		WIFI_modeSTA();
		configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
		TIME_printTime();

		WEATHER_CallParams_s *foo = API_defaultParams(latitude, longitude);
		foo->forecastTime = HOURLY;
		API_updateRainForecast(foo);
		API_printRainForecast();
		MASTER_saveForecast();
		MASTER_saveConfig();
		
		server.send(200, "text/html", SPIFFS_readFile("/main.html"));
	}else server.send(200, "text/html", SPIFFS_readFile("/not_set.html"));
}

void handle_reset(){
	EEPROM_setWifi(0);
	EEPROM_clearData();
	server.send(200, "text/html", SPIFFS_readFile("/main.html"));
	ESP.restart();
}

// connect to wifi

void handle_connectPage(){
	server.send(200, "text/html", SPIFFS_readFile("/connect.html"));
}

void handle_connect(){
  String ssid = server.arg("ssid");
  String pass = server.arg("password");
  String remember_me = server.arg("remember_me");
  
  if(remember_me == "on"){
	EEPROM_setWifi(1);
	EEPROM_setSSID(ssid);
	EEPROM_setPass(pass);
    Serial.printf("Values saved");

  } else EEPROM_setWifi(0);
  
  server.send(200, "text/html", SPIFFS_readFile("/connect.html"));
}

// add device
bool slaveWaiting = 0;
unsigned long slaveTimeout = 0;
long last_user_time;

void handle_slaveStatus(){
	String info = 'S' + String(slaveWaiting);
	server.send(200, "text/html", info);
}

void handle_newDev(){
	server.send(200, "text/html", SPIFFS_readFile("/addDev.html"));
}

uint8_t valve = 0;
void handle_addDev(){
	String pump = server.arg("pump_id");
	valve = pump.toInt();
	Serial.printf("APP add dev handle:\n\rpump id:%d\n\r",valve);
	server.send(200, "text/html", SPIFFS_readFile("/addDev.html"));
}

// settings
void handle_settings(){
	server.send(200, "text/html", SPIFFS_readFile("/settings.html"));
}

void handle_settingsCss(){
	server.send(200, "text/css", SPIFFS_readFile("/settings_style.css"));
}

void handle_timetable(){
	Serial.printf("APP timetable handle:\n\rselected hours: ");
	for(size_t t = 0;  t < 24; t++){
		timetable[t] = server.hasArg(String(t));
		if(timetable[t]) Serial.printf(" %d,",t);
	}
	Serial.printf("\n\r");
	MASTER_saveConfig();
	server.send(200, "text/html", SPIFFS_readFile("/settings.html"));
}

void handle_moisture(){
	moisture_min = server.arg("moisture").toInt();
	int watering_treshold = SOIL_MOISTURE_MIN - ((SOIL_MOISTURE_MIN - SOIL_MOISTURE_MAX)/100 * moisture_min);
	Serial.printf("APP moisture handle:\n\rmoisture_min:%d watering: %d\n\r",moisture_min, watering_treshold);
	MASTER_saveConfig();
	server.send(200, "text/html", SPIFFS_readFile("/settings.html"));
}

void handle_position(){
	latitude = server.arg("latitude").toFloat();
	longitude = server.arg("longitude").toFloat();
	Serial.printf("APP position handle:\n\rlatitude:%f,\tlongitude:%f\n\r",latitude,longitude);
	MASTER_saveConfig();
	server.send(200, "text/html", SPIFFS_readFile("/settings.html"));
}

/////////////////////////////////
time_t MACHINE_nextWake(int *wakeSource){
	// update wake stamp
	time_t nextWake = LONG_MAX;
	for(size_t k = 1; k< slaveNr; k++){
		if(slaveDevices[k].wakeStamp < nextWake) nextWake = slaveDevices[k].wakeStamp;
		Serial.printf("wake stamp %d: %d\n\r", k, slaveDevices[k].wakeStamp);
	}
	time_t timeNow = TIME_getSec();
	if(nextWake <= timeNow) {
		*wakeSource = WAKE_SENSOR;
		return timeNow;
	}

	// check watering timetable
	tm timeNowTm = *localtime(&timeNow);
	tm nextWakeTm = *localtime(&nextWake);
	int8_t wateringBetween = -1;

	for (uint8_t i = 0; i < 24; i++) {									// check timetable
		uint8_t h = ((timeNowTm.tm_hour) + i) % 24;						// start iteration since current time
		if (timetable[h]) {												// find watering event
			if ((timeNowTm.tm_hour) <= (nextWakeTm.tm_hour)) {				// before midnight
				if (h <= nextWakeTm.tm_hour && h >= timeNowTm.tm_hour) {// watering beetween
					wateringBetween = h;
					break;
				}
			}
			else {														// midnight beetween
				if (h > nextWakeTm.tm_hour && h < timeNowTm.tm_hour) {	//no watering beetween
					continue;
				}
				else {
					wateringBetween = h;
					break;
				}
			}
		}
	}
	Serial.printf("time now %s", ctime(&(timeNow)));
	Serial.printf("next wake from sensor %s", ctime(&(nextWake)));

	if (wateringBetween == -1) {
		*wakeSource = WAKE_SENSOR;
		return nextWake; 						// return sensor time
	}

	Serial.printf("watering Between %d\n\r", wateringBetween);
	struct tm wateringTm;
	wateringTm = timeNowTm;
	wateringTm.tm_min = 0;
	// 1 watering in this hour
	if(timeNowTm.tm_hour == wateringBetween){
		Serial.println("watering in this hour");
		Serial.printf("1: %d, 2: %d\n\r",(timeNowTm.tm_min), (nextWakeTm.tm_min));
		Serial.printf("1: %d, 2: %d\n\r",(timeNowTm.tm_min)/15, (nextWakeTm.tm_min)/15);
		wateringTm.tm_hour = timeNowTm.tm_hour;
		//watering check beetween
		if((timeNowTm.tm_min)/15 != (nextWakeTm.tm_min)/15){
			Serial.println("watering check beetween");
			time_t watering = timeNow;
			watering += ((15 - (timeNowTm.tm_min)%15)*60);
			*wakeSource = WAKE_VALVE;
			return watering;
		}else{
			*wakeSource = WAKE_SENSOR;
			return mktime(&nextWakeTm);
		}
	}else{	// 2 watering in future
		Serial.println("watering in next hour");
		if(timeNowTm.tm_hour < wateringBetween) wateringTm.tm_hour = wateringBetween;
		else wateringTm.tm_hour = 24 - timeNowTm.tm_hour + wateringBetween;
		*wakeSource = WAKE_VALVE;
		return mktime(&wateringTm);
	}
	*wakeSource = 0;
	return mktime(&timeNowTm);	
}

void MACHINE_configInit(DATA_MACHINE_ESP *machineState){

	WIFI_modeAP();

	// main
	server.on("/",HTTP_GET, handle_root); 
	server.on("/save", handle_save);
	server.on("/reset", handle_reset);

	// connecting page
	server.on("/connecting",HTTP_GET, handle_connectPage); 
    server.on("/connect",HTTP_POST, handle_connect);

	// add dev
	server.on("/newDev",HTTP_GET, handle_newDev); 
    server.on("/addDev",HTTP_POST, handle_addDev);
	server.on("/slaveStatus", handle_slaveStatus);

	// settings
	server.on("/settings",HTTP_GET, handle_settings);
    server.on("/settings_style.css",HTTP_GET, handle_settingsCss);
    server.on("/timetable",HTTP_POST, handle_timetable);
    server.on("/moisture",HTTP_POST, handle_moisture);
    server.on("/position",HTTP_POST, handle_position);


    server.begin();
    Serial.print("To connect with network go to this website: ");
    Serial.println(localIp);

	// NRF part
	// listen broadcast
	closeAllPipes();
	openWritingPipe(default_address);
	if(localDevice.nodeAddress[3] > 0)	openReadingPipe(1, localDevice.nodeAddress);
	startListening();
	slaveTimeout = millis();
	last_user_time = millis();
	printDetails();

	// LED pin
	pinMode(LED_OUT, OUTPUT);
}

long blink;
bool led_state;
void MACHINE_configRun(DATA_MACHINE_ESP *machineState){
	server.handleClient();
	unsigned long now = millis();
	if(WIFI_userConnected()) last_user_time = now;

	if(now > blink + 1000){
		led_state = !led_state;
		blink = now;
		digitalWrite(LED_OUT, led_state);
	}

	// Detect if there is some slave trying to connect
	if(machineState->newRx && CMD_INIT == machineState->packetBuffer.header.cmd_id){
		Serial.println("Waiting for init");
		slaveWaiting = 1;
		machineState->newRx = 0;
		slaveTimeout = now;	  
	}

	if(slaveTimeout + 10000 < now){
		slaveWaiting = 0;
	}
	
	if(valve && CMD_INIT == machineState->packetBuffer.header.cmd_id){
		if(DATA_action(&(machineState->packetBuffer))){ 
			radioWritePacket(&(machineState->packetBuffer));
			valve = 0;
		}
  }

  if(change_state || (+ 60000 < now)){					// next state
	WIFI_disconnect();
	digitalWrite(LED_OUT, 0);
	MACHINE_changeState(S_WAIT, machineState);
	change_state = 0;
  }
}

RTC_DATA_ATTR int wakeSource = 0;
void MACHINE_waitInit(DATA_MACHINE_ESP *machineState){
	radio.powerDown();
	WIFI_disconnect();
	// update wake stamp
	time_t nextWake = MACHINE_nextWake(&wakeSource);
	slaveDevices[0].wakeStamp = nextWake;
	MASTER_saveConfig();
	MASTER_saveForecast();
	Serial.printf("next wake stamp %s", ctime(&(slaveDevices[0].wakeStamp)));
	time_t sleep_time = nextWake - TIME_getSec();

	if(sleep_time > 0){
		uint64_t timer_value = sleep_time;
		Serial.printf("Sleep for %d", timer_value);
		Serial.printf("Next wake by %d\n\r",wakeSource);
		VALVE_offState();
		esp_sleep_enable_timer_wakeup(timer_value * 1000000 );
		esp_sleep_enable_ext0_wakeup((gpio_num_t)CONFIG_INPUT, 0);
		esp_deep_sleep_start();
		
	} else MACHINE_changeState(S_PACKET_HANDLE_LISTEN, machineState);
}

void MACHINE_waitRun(DATA_MACHINE_ESP *machineState){
	MACHINE_changeState(S_PACKET_HANDLE_LISTEN, machineState);
}

//listen group multicast
void MACHINE_packetHandleListenInit(DATA_MACHINE_ESP *stateMachine){
	closeAllPipes();
	openWritingPipe(local_group);
	if(localDevice.nodeAddress[3] > 0)	openReadingPipe(1, localDevice.nodeAddress);
	startListening();
	printDetails();
}

void MACHINE_packetHandleListenRun(DATA_MACHINE_ESP *stateMachine){
	if(stateMachine->newRx){
		if(stateMachine->packetBuffer.header.cmd_id == CMD_PING){
			uint8_t slave_id = stateMachine->packetBuffer.node_id;
			if(DATA_action(&(stateMachine->packetBuffer))){
				DATA_printPacket(&stateMachine->packetBuffer);
				openWritingPipe(slaveDevices[slave_id-1].nodeAddress);
				Serial.println(slave_id);
				DATA_printDEVICE(slaveDevices[slave_id-1]);
				radioWritePacket(&(stateMachine->packetBuffer));
				delay(1000);
				radioWritePacket(&(stateMachine->packetBuffer));
				MACHINE_changeState(S_PACKET_HANDLE_DATA, stateMachine);
			}
		}
		stateMachine->newRx = 0;
	}
}

// listen only selected slave and yout master
void MACHINE_packetHandleDataInit(DATA_MACHINE_ESP *stateMachine){
	startListening();
	printDetails();
}

void MACHINE_packetHandleDataRun(DATA_MACHINE_ESP *stateMachine){
	delay(1000);
	DATA_sendDataReq();
	if(stateMachine->newRx && stateMachine->packetBuffer.header.cmd_id == CMD_GET_DATA && !stateMachine->packetBuffer.header.request){
		Serial.println("Data income");
		uint8_t slave_id = stateMachine->packetBuffer.node_id;
		memcpy(&(slaveDevices[slave_id-1].sensorValues), stateMachine->packetBuffer.payload,sizeof(DATA_VALUES));
		Serial.printf("temp: %d, \thum: %d, \tmois:%d\n\r", slaveDevices[slave_id-1].sensorValues.temperature, slaveDevices[slave_id-1].sensorValues.humidity, slaveDevices[slave_id-1].sensorValues.moisture);
		MACHINE_changeState(S_PACKET_HANDLE_SLEEP, stateMachine);

		stateMachine->newRx = 0;
	}
}

int DATA_getSleepTime(){
	struct tm timeinfo = TIME_getTime();
	if(timetable[timeinfo.tm_hour]){
		return SAMPLE_INTERVAL_WATERING;
	}else if(timeinfo.tm_hour == 23){
		if(timetable[0]) return (60 - timeinfo.tm_min + (timeinfo.tm_min%SAMPLE_INTERVAL_WATERING));
	}else if(timetable[timeinfo.tm_hour + 1]){
		return (60 - timeinfo.tm_min + (timeinfo.tm_min%SAMPLE_INTERVAL_WATERING));
	}
	return SAMPLE_INTERVAL;
}

// listen only selected slave and yout master
void MACHINE_packetHandleSleepInit(DATA_MACHINE_ESP *stateMachine){
	startListening();
	printDetails();
}

void MACHINE_packetHandleSleepRun(DATA_MACHINE_ESP *stateMachine){
	delay(1000);
	uint8_t sleeeep = DATA_getSleepTime();
	DATA_sendSleep(sleeeep);
	if(stateMachine->newRx){
		if(stateMachine->packetBuffer.header.cmd_id == CMD_GO_TO_SLEEP && !stateMachine->packetBuffer.header.request){
			Serial.println("sleep response");
			uint8_t slave_id = stateMachine->packetBuffer.node_id;
			slaveDevices[slave_id-1].sleepTime = sleeeep;
			time_t wakeStamp = TIME_getSec() + (60* sleeeep);
			slaveDevices[slave_id-1].wakeStamp = wakeStamp;
			Serial.printf("Wake Stamp %s ", ctime(&wakeStamp));
			MACHINE_changeState(S_WAIT, stateMachine);
		}
		stateMachine->newRx = 0;
	}
}

void MACHINE_initMachine(DATA_MACHINE_ESP *stateMachine){
	if(!SPIFFS_init()) return;
	EEPROM_init();
  	VALVE_init();
	memset(stateMachine, 0, sizeof(DATA_MACHINE_ESP));
	MASTER_addNewNode(); 		//add device as a node

	// if conf in memory
	if(MASTER_loadConfig()){
		MASTER_loadForecast();
		Serial.printf("Slaves in network: %d\n\r",slaveNr);
		Serial.printf("Wake Source: %d\n\r",wakeSource);
		if(!digitalRead(CONFIG_INPUT)) MACHINE_changeState(S_CONFIG, stateMachine);	// if config mode

		else if(wakeSource != 0){													// chack wake source to detect what it should do after waking up
			configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
			TIME_printTime();
			if(wakeSource == WAKE_VALVE) MACHINE_changeState(S_VALVE, stateMachine);
			else MACHINE_changeState(S_PACKET_HANDLE_LISTEN, stateMachine);
			wakeSource = 0;
		}else{
			if(!WIFI_modeSTA()) MACHINE_changeState(S_CONFIG, stateMachine);
			else{
				configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
				TIME_printTime();

				WEATHER_CallParams_s *foo = API_defaultParams(latitude, longitude);
				foo->forecastTime = HOURLY;
				API_updateRainForecast(foo);
				API_printRainForecast();
				API_printLastTimestamp();
				float rain = API_rainInNext24h();
				Serial.printf("Sum of rain in 24h: %f\n\r",rain);

				WIFI_disconnect();

				MACHINE_changeState(S_WAIT, stateMachine);
			}
		}
	// no conf
	} else{
		MACHINE_changeState(S_CONFIG, stateMachine);
	}
}

void MACHINE_valveInit(DATA_MACHINE_ESP *stateMachine){
	Serial.println("Valve state init");
	
	VALVE_closeAll();
	TIME_printTime();
	if(API_needUpdate()){			// update forecast
		WIFI_modeSTA();

		WEATHER_CallParams_s *foo = API_defaultParams(latitude, longitude);
		foo->forecastTime = HOURLY;
		API_updateRainForecast(foo);
		API_printRainForecast();
		WIFI_disconnect();
	}

	float rain = API_rainInNext24h();
	if(rain < RAIN_MIN){					// watering enabled because no rain
		int watering_treshold = SOIL_MOISTURE_MIN - ((SOIL_MOISTURE_MIN - SOIL_MOISTURE_MAX)/100 * moisture_min);
		uint8_t worst_valve = 0;
		uint16_t worst_case = 0;
		for(size_t k = 1; k < slaveNr; k++){
			if(slaveDevices[k].sensorValues.moisture < SOIL_MOISTURE_MIN && slaveDevices[k].sensorValues.moisture > SOIL_MOISTURE_MAX){			// value in range
				if(slaveDevices[k].sensorValues.moisture > watering_treshold && slaveDevices[k].sensorValues.moisture > worst_case){				// find worst case
					worst_case = slaveDevices[k].sensorValues.moisture;
					worst_valve = slaveDevices[k].pumpId;
				}
			}
		}
		Serial.printf("Watering valve: %d\n\r", worst_valve);
		VALVE_action(worst_valve, OPEN);
	}



}

void MACHINE_valveRun(DATA_MACHINE_ESP *stateMachine){
	MACHINE_changeState(S_WAIT, stateMachine);
}

void MACHINE_changeState(MACHINE_STATE_ESP setState, DATA_MACHINE_ESP *machineState){
	switch(setState){
		case S_WAIT:
		Serial.printf("Init State\t S_WAIT\r\n");
		MACHINE_waitInit(machineState);
		machineState->state = setState;
		break;

		case S_CONFIG:
		Serial.printf("Init State\t S_CONFIG\r\n");
		MACHINE_configInit(machineState);
		machineState->state = setState;
		break;

		case S_PACKET_HANDLE_LISTEN:
		Serial.printf("Init State\t S_PACKET_HANDLE_LISTEN\r\n");
		MACHINE_packetHandleListenInit(machineState);
		machineState->state = setState;
		break;

		case S_PACKET_HANDLE_DATA:
		Serial.printf("Init State\t S_PACKET_HANDLE_DATA\r\n");
		MACHINE_packetHandleDataInit(machineState);
		machineState->state = setState;
		break;

		case S_PACKET_HANDLE_SLEEP:
		Serial.printf("Init State\t S_PACKET_HANDLE_SLEEP\r\n");
		MACHINE_packetHandleSleepInit(machineState);
		machineState->state = setState;
		break;

		case S_VALVE:
		Serial.printf("Init State\t S_VALVE\r\n");
		MACHINE_valveInit(machineState);
		machineState->state = setState;
		break;
		
	}
}

void MACHINE_stateMachine(DATA_MACHINE_ESP *machineState){
	switch(machineState->state){
		case S_WAIT:
		MACHINE_waitRun(machineState);
		break;

		case S_CONFIG:
		MACHINE_configRun(machineState);
		break;
		
		case S_PACKET_HANDLE_LISTEN:
		MACHINE_packetHandleListenRun(machineState);
		break;

		case S_PACKET_HANDLE_DATA:
		MACHINE_packetHandleDataRun(machineState);
		break;

		case S_PACKET_HANDLE_SLEEP:
		MACHINE_packetHandleSleepRun(machineState);
		break;

		case S_VALVE:
		MACHINE_valveRun(machineState);
		break;
		
	}
}









// BE MASTER

void DATA_printDEVICE(DEVICE Device){
	Serial.printf("Connected: %d, NodeID: %d, Valve: %d, ", Device.connected, Device.nodeId, Device.pumpId);
	Serial.printf("ADDR: %X %X %X %X %X\n\r", Device.nodeAddress[0], Device.nodeAddress[1], Device.nodeAddress[2], Device.nodeAddress[3], Device.nodeAddress[4]);
}

uint8_t MASTER_addNewNode(){
	if (SLAVE_NR_MAX > slaveNr){
		slaveDevices[slaveNr].connected = 1;
		memcpy(slaveDevices[slaveNr].nodeAddress, local_address, sizeof(local_address));
		slaveDevices[slaveNr].nodeAddress[4] = slaveNr+1;
		slaveDevices[slaveNr].nodeId = slaveNr+1;
		slaveDevices[slaveNr].pumpId = valve;
		slaveDevices[slaveNr].wakeStamp = 1;
		DATA_printDEVICE(slaveDevices[slaveNr]);
		slaveNr++;
		return (slaveNr-1);
	}else return 0;				//If tab is full
}

void MASTER_saveConfig(){
	CONFIG buffer;
	memcpy(&buffer.localDevice, &localDevice, sizeof(DEVICE));
	memcpy(buffer.slaveDevices, slaveDevices, SLAVE_NR_MAX * sizeof(DEVICE));
	memcpy(buffer.timetable, timetable, sizeof(bool)*24);
	memcpy(&buffer.latitude, &latitude, sizeof(latitude));
	memcpy(&buffer.longitude, &longitude, sizeof(longitude));
	memcpy(&buffer.moisture_min, &moisture_min, sizeof(moisture_min));
	buffer.slaveNr = slaveNr;
	EEPROM_setData(&buffer,sizeof(CONFIG));
}

void MASTER_saveForecast(){
	String data;
	API_getLastTimestamp(&data);
	SPIFFS_writeFile("/last_timestamp.txt", data);
	API_getRainForecast(&data);
	SPIFFS_writeFile("/rain_forecast.txt", data);
}

bool MASTER_loadConfig(){
	CONFIG buffer;
	if(EEPROM_getData(&buffer,sizeof(CONFIG))){
		memcpy(&localDevice, &buffer.localDevice, sizeof(DEVICE));
		memcpy(slaveDevices, buffer.slaveDevices, SLAVE_NR_MAX * sizeof(DEVICE));
		memcpy(timetable, buffer.timetable, sizeof(bool)*24);
		memcpy(&latitude, &buffer.latitude, sizeof(latitude));
		memcpy(&longitude, &buffer.longitude, sizeof(longitude));
		memcpy(&moisture_min, &buffer.moisture_min, sizeof(moisture_min));
		slaveNr = buffer.slaveNr;
		return 1;
	}
	return 0;
}

void MASTER_loadForecast(){
	String data = SPIFFS_readFile("/last_timestamp.txt");
	API_setLastTimestamp(&data);
	data = SPIFFS_readFile("/rain_forecast.txt");
	API_setRainForecast(&data);
	Serial.println("LOAD FORECAST");
	Serial.println(data);
	Serial.println("EOF");
}

void WIFI_modeAP(){
	Serial.println("AP mode");
	WiFi.mode(WIFI_STA);
    WiFi.disconnect();
    WiFi.mode(WIFI_AP);
    delay(100);

	WiFi.softAPConfig(localIp, gatewayAP, subnet);
    WiFi.softAP(soft_ap_ssid);
}

bool WIFI_modeSTA(){

  	char ssid_buff[32];
  	char pass_buff[64];
	EEPROM_getSSID().toCharArray(ssid_buff,32);
	EEPROM_getPass().toCharArray(pass_buff,64);

	Serial.printf("Connecting to %s ", ssid_buff);
	WiFi.begin(ssid_buff, pass_buff);
	unsigned long timestamp = millis();
	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		Serial.print(".");
		if(timestamp + 10000 < millis()) return 0;
	}
	Serial.println(" CONNECTED");
	return 1;
}

void WIFI_disconnect(){
	WiFi.disconnect(true);
	WiFi.mode(WIFI_OFF);
}

bool WIFI_userConnected(){
	return (bool) WiFi.softAPgetStationNum();
}
